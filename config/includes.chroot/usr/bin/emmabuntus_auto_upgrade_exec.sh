#!/bin/bash


# Emmabuntus_auto_upgrade_exec.sh --
#
#   This file permits to automatically update security package
#   for Emmabuntüs distribution.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################


clear

auto_upgrade_file=/etc/apt/apt.conf.d/20auto-upgrades

# Récupération de l'argument après le passage en root
enable_auto_upgrade=$1


if [[ ${enable_auto_upgrade} == "1" ]] ; then


    sed s/^APT::Periodic::Update-Package-Lists[^$]*/APT::Periodic::Update-Package-Lists\ \"1\"\;/ ${auto_upgrade_file} | sudo tee  ${auto_upgrade_file}.tmp
    sed s/^APT::Periodic::Unattended-Upgrade[^$]*/APT::Periodic::Unattended-Upgrade\ \"1\"\;/ ${auto_upgrade_file}.tmp | sudo tee  ${auto_upgrade_file}
    sudo rm ${auto_upgrade_file}.tmp

elif [[ ${enable_auto_upgrade} == "0" ]] ; then

    sed s/^APT::Periodic::Update-Package-Lists[^$]*/APT::Periodic::Update-Package-Lists\ \"0\"\;/ ${auto_upgrade_file} | sudo tee  ${auto_upgrade_file}.tmp
    sed s/^APT::Periodic::Unattended-Upgrade[^$]*/APT::Periodic::Unattended-Upgrade\ \"0\"\;/ ${auto_upgrade_file}.tmp | sudo tee  ${auto_upgrade_file}
    sudo rm ${auto_upgrade_file}.tmp

fi

exit 0
