#! /bin/bash

# Emmabuntus_config_postinstall.sh --
#
#   This file permits config of postinstall to choise between:
#    - the old Xubuntu menu or Whisker menu (Emmabuntus_choice_panel_menu.sh)
#    - two sets of images for screensaver (Emmabuntus_choice_screensaver_images.sh)
#    - wallpaper type among a set of wallpapers made by  Odysseus Libre :
#      http://odysseuslibre.be
#      These wallpapers are under the Licence Art Libre 1.3 (LAL 1.3)
#      (emmabuntus_choice_screensaver_images.sh)
#    - autostart_gspeech.sh
#    - autologin_lightdm.sh
#
#   Created on 2010-23 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


clear

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


repertoire_script=/usr/bin

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config_postinstall=${repertoire_emmabuntus}/init_config_postinstall.txt


# Fichier config menu
repertoire_panel=~/.config/xfce4/xfconf/xfce-perchannel-xml
choix_menu="Whisker"
choix_menu_visible="true"

# Fichiers config économiseur d'écran
repertoire_image_emmabuntus=\\/usr\\/share\\/xfce4\\/backdrops\\/screensaver
repertoire_image_ubuntu=\\/usr\\/share\\/xfce4\\/backdrops\\/screen_ubuntu
screensaver_config_file=~/.xscreensaver
choix_screen_saver="Ubuntu"

# Fichiers config autologin_lightdm
file_lightdm=/etc/lightdm/lightdm.conf

if [[ ${USER} == root ]] ; then
tmp_postinstall=/root/.cache/postinstall
else
tmp_postinstall=/home/${USER}/.cache/postinstall
fi

# Fichiers config fond d'écran
if ps -A | grep "lxqt-session" ; then
    repertoire_wallpaper_emmabuntus="/usr/share/lxqt/themes/emmabuntus"
else
    repertoire_wallpaper_emmabuntus="/usr/share/xfce4/backdrops"
fi


choix_wallpaper="Classic"

fond_ecran_en="framasoft_wallpaper_emmabuntus_classic"
fond_ecran_fr="framasoft_fond_ecran_accueil"

CST_RATIO_5_4=125    ; # 1280x1024
CST_RATIO_4_3=133    ; # actual value = 1.333 ; # 1280x960
CST_RATIO_16_10=166  ; # 1280x800
CST_RATIO_16_9=177   ; # actual value = 1.777 ; # 1280x720


# fichiers gestion imagette affichage
if ! test -d  ${tmp_postinstall}; then mkdir ${tmp_postinstall}; fi
lnk_imagette_economiseur_ecran="${tmp_postinstall}/imagette_economiseur_ecran"
lnk_imagette_fond_ecran="${tmp_postinstall}/imagette_fond_ecran"
export lnk_imagette_economiseur_ecran lnk_imagette_fond_ecran


# Fichier config autologin
repertoire_emmabuntus=~/.config/emmabuntus
fichier_init_config_autologin=${repertoire_emmabuntus}/autologin_lightdm.txt
# Utilisateur n'appartient pas au group sudo
if [[ $(groups | grep sudo) == "" ]] ; then
    autologin_visible="false"
    autologin="false"
else
    autologin_visible="true"
    autologin="true"
fi

# Fichier config gspeech
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config_XFCE=${repertoire_emmabuntus}/init_gspeech_XCFE.txt
fichier_init_config_LXDE=${repertoire_emmabuntus}/init_gspeech_LXDE.txt
fichier_init_config_OpenBox=${repertoire_emmabuntus}/init_gspeech_OpenBox.txt
fichier_init_config_LXQT=${repertoire_emmabuntus}/init_gspeech_LXQT.txt
gspeech="false"

# Fichier config bluetooth
fichier_init_config_bluetooth=${repertoire_emmabuntus}/init_bluetooth.txt
bluetooth="false"


# Changement du nom du fichier_init_config_postinstall par session
if ps -A | grep "xfce4-session" ; then
    fichier_init_config_postinstall=${repertoire_emmabuntus}/xfce_init_config_postinstall.txt
elif ps -A | grep "lxsession" ; then
    fichier_init_config_postinstall=${repertoire_emmabuntus}/lxde_init_config_postinstall.txt
elif ps -A | grep "openbox" ; then
    fichier_init_config_postinstall=${repertoire_emmabuntus}/openbox_init_config_postinstall.txt
elif ps -A | grep "lxqt-session" ; then
    fichier_init_config_postinstall=${repertoire_emmabuntus}/lxqt_init_config_postinstall.txt
fi


if [[ ! ( -f $fichier_init_config_postinstall) ]]
then


msg_classic_wallpaper="Classic Emmabuntüs"
msg_new_wallpaper0a="Emmabuntus-Ice"
msg_new_wallpaper0b="Emmabuntus-Blumine"
msg_new_wallpaper1="Emmabuntus-Wallpaper-01"
msg_new_wallpaper2="Emmabuntus-Wallpaper-02"
msg_new_wallpaper3="Emmabuntus-Wallpaper-03"
msg_new_wallpaper4="Emmabuntus-Wallpaper-04"
msg_new_wallpaper5="Emmabuntus-Wallpaper-05"

msg_old_image="Emmabuntüs"
msg_new_image1="Ubuntu"
msg_screensaver_off="Off"


# Detection de la présence de l'adapateur Bluetooth

bluetooth_present=$(hciconfig |grep Type |cut -d ':' -f1)

if [ "$bluetooth_present" = "" ]; then
    echo "Bluetooth not found"
    bluetooth_visible="false"
    HEIGHT_WINDOW=400
else
    echo "Bluetooth found"
    bluetooth_visible="true"
    HEIGHT_WINDOW=440
fi

if ps -A | grep "lxqt-session" ; then
    echo "LXQt found"
    choix_menu_visible="false"
    HEIGHT_WINDOW=$(($HEIGHT_WINDOW - 40))
fi



${repertoire_script}/emmabuntus_config_postinstall_func.sh ECONOMISEUR_ECRAN ${msg_new_image1}
${repertoire_script}/emmabuntus_config_postinstall_func.sh FOND_ECRAN ${msg_classic_wallpaper}


 export WINDOW_DIALOG='<window title="'$(eval_gettext 'Desktop configuration')'" icon-name="gtk-dialog-question" width_request="700" height_request="'${HEIGHT_WINDOW}'" deletable="false" maximize_initially="true" resizable="false">
 <vbox spacing="0">
 <frame>
 <hbox spacing="0" space-expand="true" space-fill="true" visible="'${choix_menu_visible}'">
  <text use-markup="true" wrap="false" xalign="0" justify="3">
  <input>echo "\n'$(eval_gettext 'Please select the application menu version that best suits your needs: ')'" | sed "s%\\\%%g"</input>
  </text>
  <comboboxtext allow-empty="false" value-in-list="true" tooltip-text="'$(eval_gettext 'Classic menu is more suited for older computers. The new Whisker menu is more ergonomic, but requires more RAM.')'">
   <variable>choix_menu</variable>
   <default>'${choix_menu}'</default>
   <item>"'$(eval_gettext 'Classic')'"</item>
   <item>"'$(eval_gettext 'Whisker')'"</item>
  </comboboxtext>
 </hbox>

 <hbox spacing="0" space-expand="true" space-fill="true">
  <text use-markup="true" wrap="false" xalign="0" justify="3">
  <input>echo "\'$(eval_gettext 'Please select the screen saver of your choice: ')'" | sed "s%\\\%%g"</input>
  </text>
  <comboboxtext allow-empty="false" value-in-list="true" tooltip-text="'$(eval_gettext 'Images of the Abbé Pierre Foundation campaign against inadequate housing or from Ubuntu wallpaper set.')'">
   <variable>choix_screen_saver</variable>
   <default>'${msg_new_image1}'</default>
   <item>'${msg_old_image}'</item>
   <item>'${msg_new_image1}'</item>
   <item>'${msg_screensaver_off}'</item>
   <action>'${repertoire_script}'/emmabuntus_config_postinstall_func.sh ECONOMISEUR_ECRAN ${choix_screen_saver}</action>
   <action>refresh:imagette_economiseur_ecran</action>
  </comboboxtext>
 </hbox>

 <hbox spacing="0" space-expand="true" space-fill="true">
  <text use-markup="true" wrap="false" xalign="0" justify="3">
  <input>echo "\n'$(eval_gettext 'Please select the wallpaper of your choice: ')'" | sed "s%\\\%%g"</input>
  </text>
  <comboboxtext allow-empty="false" value-in-list="true" tooltip-text="'$(eval_gettext 'Free wallpapers taken from: L.L. de Mars, Odysseus Libre.')'">
    <variable>choix_wallpaper</variable>
    <item>'${msg_classic_wallpaper}'</item>
    <item>'${msg_new_wallpaper0a}'</item>
    <item>'${msg_new_wallpaper0b}'</item>
    <item>'${msg_new_wallpaper1}'</item>
    <item>'${msg_new_wallpaper2}'</item>
    <item>'${msg_new_wallpaper3}'</item>
    <item>'${msg_new_wallpaper4}'</item>
    <item>'${msg_new_wallpaper5}'</item>
    <action>'${repertoire_script}'/emmabuntus_config_postinstall_func.sh FOND_ECRAN ${choix_wallpaper}</action>
    <action>refresh:imagette_fond_ecran</action>
  </comboboxtext>
 </hbox>

 <hseparator space-expand="true" space-fill="true"></hseparator>

 <hbox spacing="0" space-expand="true" space-fill="true">
  <pixmap>
  <variable export="true">imagette_economiseur_ecran</variable>
  <height>150</height>
  <input file>'${lnk_imagette_economiseur_ecran}'</input>
  </pixmap>

  <pixmap>
  <variable export="true">imagette_fond_ecran</variable>
  <height>150</height>
  <input file>'${lnk_imagette_fond_ecran}'</input>
  </pixmap>
 </hbox>

 </frame>

  <checkbox height_request="40" active="'${bluetooth}'" visible="'${bluetooth_visible}'">
    <variable>bluetooth</variable>
    <label>"'$(eval_gettext 'Enable automatic launch of Bluetooth')'"</label>
  </checkbox>
  <checkbox height_request="40" active="'${gspeech}'">
    <variable>gspeech</variable>
    <label>"'$(eval_gettext 'Start gSpeech (Text to Speech) at system startup')'"</label>
  </checkbox>
  <checkbox height_request="40" active="'${autologin}'" visible="'${autologin_visible}'">
    <variable>autologin</variable>
    <label>"'$(eval_gettext 'Enable automatic login')'"</label>
  </checkbox>
  <hseparator space-expand="true" space-fill="true"></hseparator>
  <hbox space-expand="false" space-fill="false">
  <button can-default="true" has-default="true" use-stock="true" is-focus="true">
  <label>gtk-ok</label>
  <action>exit:OK</action>
  </button>
  </hbox>
 </vbox>
 </window>'


MENU_DIALOG="$(gtkdialog --center --program=WINDOW_DIALOG)"

eval ${MENU_DIALOG}
echo "MENU_DIALOG=${MENU_DIALOG}"

echo "Choise Menu=${choix_menu}"
echo "Choise Screen saver=${choix_screen_saver}"
echo "Choise Wallpaper=${choix_wallpaper}"
echo "Choise bluetooth=${bluetooth}"
echo "Choise gspeech=${gspeech}"
echo "Choise autologin=${autologin}"

if [ ${EXIT} == "OK" ]
then

    # Application choix menu

    if [[ $choix_menu == *Whisker* ]]
    then
        if [[ ! $(xfconf-query -c xfce4-panel -p /plugins/plugin-1 | grep "whiskermenu") ]] ; then
            xfconf-query -v -c xfce4-panel -p /plugins/plugin-1 -s "whiskermenu"
            pkill xfconfd
            xfce4-panel --restart
        fi
    else
        if [[ ! $(xfconf-query -c xfce4-panel -p /plugins/plugin-1 | grep "applicationsmenu") ]] ; then
            xfconf-query -v -c xfce4-panel -p /plugins/plugin-1 -s "applicationsmenu"
            pkill xfconfd
            xfce4-panel --restart
        fi
    fi


    # Application choix économiseur d'écran

    if [[ $choix_screen_saver == *Emmabuntüs* ]]
    then
        sed s/^imageDirectory:[^$]*/imageDirectory:\\t${repertoire_image_emmabuntus}/ ${screensaver_config_file} > ${screensaver_config_file}.tmp1
        sed s/^mode:[^$]*/mode:\\tone/ ${screensaver_config_file}.tmp1 > ${screensaver_config_file}.tmp2
        sed s/^selected:[^$]*/selected:\\t143/ ${screensaver_config_file}.tmp2 > ${screensaver_config_file}.tmp
        rm ${screensaver_config_file}.tmp1
        rm ${screensaver_config_file}.tmp2
    elif [[ $choix_screen_saver == *Ubuntu* ]]
    then
        sed s/^imageDirectory:[^$]*/imageDirectory:\\t${repertoire_image_ubuntu}/ ${screensaver_config_file} > ${screensaver_config_file}.tmp1
        sed s/^mode:[^$]*/mode:\\tone/ ${screensaver_config_file}.tmp1 > ${screensaver_config_file}.tmp2
        sed s/^selected:[^$]*/selected:\\t143/ ${screensaver_config_file}.tmp2 > ${screensaver_config_file}.tmp
        rm ${screensaver_config_file}.tmp1
        rm ${screensaver_config_file}.tmp2
    else
        sed s/^mode:[^$]*/mode:\\toff/ ${screensaver_config_file} > ${screensaver_config_file}.tmp
    fi

    cp ${screensaver_config_file}.tmp ${screensaver_config_file}
    rm ${screensaver_config_file}.tmp

    # Application choix du fond d'écran
    # Détermination de la résolution de l'écran pour le choix du fond d'écran


    if [[ $LANG == fr* ]]; then
        fond_ecran=${fond_ecran_fr}
    else
        fond_ecran=${fond_ecran_en}
    fi


    x=`xwininfo -root | grep Width: |cut -d: -f 2`
    y=`xwininfo -root | grep Height: |cut -d: -f 2`

    ratio_ecran=$(($(( $x * 100 ))/$y))

    if test $ratio_ecran -le $(( $(( $CST_RATIO_4_3 + $CST_RATIO_5_4 )) / 2 ))
    then
    ecran="5_4"
    ecran_wallpaper="1280x960"

    elif test $ratio_ecran -le $(( $(( $CST_RATIO_16_10 + $CST_RATIO_4_3 )) / 2 ))
    then
    ecran="4_3"
    ecran_wallpaper="1280x960"

    elif test $ratio_ecran -le $(( $(( $CST_RATIO_16_9 + $CST_RATIO_16_10 )) / 2 ))
    then
    ecran="16_10"
    ecran_wallpaper="1600x900"

    elif test $ratio_ecran -ge $(( $(( $CST_RATIO_16_9 + $CST_RATIO_16_10 )) / 2 ))
    then
    ecran="16_9"
    ecran_wallpaper="1600x900"

    else
    echo "Ecran résolution inconnue !!!"
    ecran="4_3"
    ecran_wallpaper="1280x960"

    fi

    echo "Format de l'ecran = $ecran & $ecran_wallpaper"


    if [[ $choix_wallpaper == *Classi* || $choix_wallpaper == *Wallpaper* || $choix_wallpaper == *Emmabuntus-Ice* || $choix_wallpaper == *Emmabuntus-Blumine* ]]
    then

        if [[ $choix_wallpaper == *Classi* ]] ; then
            wallpaper_file=${repertoire_wallpaper_emmabuntus}/${fond_ecran}_${ecran}.jpg
        elif [[ $choix_wallpaper == *Emmabuntus-Ice* ]] ; then
            wallpaper_file=${repertoire_wallpaper_emmabuntus}/Emmabuntus_Ice_background.svg
        elif [[ $choix_wallpaper == *Emmabuntus-Blumine* ]] ; then
            wallpaper_file=${repertoire_wallpaper_emmabuntus}/Emmabuntus_logo_background_blumine.svg
        elif [[ $choix_wallpaper == *Wallpaper* ]] ; then
            wallpaper_file=${repertoire_wallpaper_emmabuntus}/${choix_wallpaper}-${ecran_wallpaper}.svg
        else
            wallpaper_file=${repertoire_wallpaper_emmabuntus}/framasoft_fond_ecran_accueil.jpg
        fi

        if test -f ${wallpaper_file}
        then

            if ps -A | grep "lxqt-session" ; then

                pcmanfm-qt --set-wallpaper ${wallpaper_file}
                pcmanfm-qt --wallpaper-mode stretch

            else

                for config_line in $(xfconf-query -v -c xfce4-desktop -l | grep last-single-image | cut -d ' ' -f 1) ; do xfconf-query -v -c xfce4-desktop -p ${config_line} -s "${wallpaper_file}" ; done

                for config_line in $(xfconf-query -v -c xfce4-desktop -l | grep image-path | cut -d ' ' -f 1) ; do xfconf-query -v -c xfce4-desktop -p ${config_line} -s "${wallpaper_file}" ; done

                for config_line in $(xfconf-query -v -c xfce4-desktop -l | grep last-image | cut -d ' ' -f 1) ; do xfconf-query -v -c xfce4-desktop -p ${config_line} -s "${wallpaper_file}" ; done

                for config_line in $(xfconf-query -v -c xfce4-desktop -l | grep image-style | cut -d ' ' -f 1) ; do xfconf-query -c xfce4-desktop -p ${config_line} -s 5 ; done

            fi

        fi

    fi

    # Application autologin

    if [[ $autologin == "true" ]]
    then

        if [[ $(cat ${file_lightdm} | grep "^autologin-user=${USER}") ]]
        then

            echo "already active ${USER} autologin"

        else

            sudo /usr/bin/autologin_lightdm_exec.sh

            echo "Activated autologin LightDM" > ${fichier_init_config_autologin}
            echo "Activated autologin LightDM" >> ${fichier_init_config_postinstall}

        fi

    else

        echo "No Activation autologin LightDM" > ${fichier_init_config_autologin}
        echo "No Activation autologin LightDM" >> ${fichier_init_config_postinstall}

    fi

    # Application gspeech

    #  chargement des variables d'environnement
    . ${env_emmabuntus}

    fichier_init_config_gspeech=$fichier_init_config_XFCE

    if ps -A | grep "xfce4-session"
    then
        fichier_init_config_gspeech=$fichier_init_config_XFCE
    elif ps -A | grep "lxsession"
    then
        fichier_init_config_gspeech=$fichier_init_config_LXDE
    elif ps -A | grep "openbox"
    then
        fichier_init_config_gspeech=$fichier_init_config_OpenBox
    elif ps -A | grep "lxqt-session"
    then
        fichier_init_config_gspeech=$fichier_init_config_LXQT
    fi

    if [[ $gspeech == "true" ]]
    then

        echo "# gSpeech enabled" >> $fichier_init_config_gspeech
        echo "# gSpeech enabled" >> ${fichier_init_config_postinstall}
        echo "# gSpeech enabled"

        if ps -A | grep "xfce4-session"
        then

            sed s/"gSpeech=\"[0-9]*\""/"gSpeech=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        elif ps -A | grep "lxsession"
        then

            sed s/"gSpeech_LXDE=\"[0-9]*\""/"gSpeech_LXDE=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        elif ps -A | grep "openbox"
        then

            sed s/"gSpeech_OpenBox=\"[0-9]*\""/"gSpeech_OpenBox=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp


        elif ps -A | grep "lxqt-session"
        then

            sed s/"gSpeech_LXQT=\"[0-9]*\""/"gSpeech_LXQT=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        fi

        cp ${env_emmabuntus}.tmp ${env_emmabuntus}
        rm ${env_emmabuntus}.tmp

        if test -f ~/.cache/gSpeech/gspeech.pid
        then
            rm ~/.cache/gSpeech/gspeech.pid
        fi

        kill -9 $(pgrep gspeech)
        gspeech &

    else

        echo "# gSpeech diseabled" >> $fichier_init_config_gspeech
        echo "# gSpeech diseabled" >> ${fichier_init_config_postinstall}
        echo "# gSpeech diseabled"

        if test -f ~/.cache/gSpeech/gspeech.pid
        then
            rm ~/.cache/gSpeech/gspeech.pid
        fi
        kill -9 $(pgrep gspeech)


        if ps -A | grep "xfce4-session"
        then

            sed s/"gSpeech=\"[0-9]*\""/"gSpeech=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        elif ps -A | grep "lxsession"
        then

            sed s/"gSpeech_LXDE=\"[0-9]*\""/"gSpeech_LXDE=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        elif ps -A | grep "openbox"
        then

            sed s/"gSpeech_OpenBox=\"[0-9]*\""/"gSpeech_OpenBox=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        elif ps -A | grep "lxqt-session"
        then

            sed s/"gSpeech_LXQT=\"[0-9]*\""/"gSpeech_LXQT=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        fi

        cp ${env_emmabuntus}.tmp ${env_emmabuntus}
        rm ${env_emmabuntus}.tmp

    fi

    # Application Bluetooth

    if [[ $bluetooth == "true" ]]
    then

        echo "# Bluetooth enabled" >> $fichier_init_config_bluetooth
        echo "# Bluetooth enabled" >> ${fichier_init_config_postinstall}
        echo "# Bluetooth enabled"

        sed s/"Bluetooth=\"[0-9]*\""/"Bluetooth=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        if [[ ! $(ps -A | grep blueman-applet) ]] ; then
            echo "blueman-applet not active"
            blueman-applet &
        fi


    else

        echo "# Bluetooth diseabled" >> $fichier_init_config_gspeech
        echo "# Bluetooth diseabled" >> ${fichier_init_config_postinstall}
        echo "# Bluetooth diseabled"

        sed s/"Bluetooth=\"[0-9]*\""/"Bluetooth=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

    fi

    cp ${env_emmabuntus}.tmp ${env_emmabuntus}
    rm ${env_emmabuntus}.tmp

fi

fi
