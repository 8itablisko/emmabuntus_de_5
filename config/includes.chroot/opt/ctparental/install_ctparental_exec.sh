#! /bin/bash

# install_ctparental_exec.sh --
#
#   This file permits to install CTparental software for the Emmabuntus Distrib.
#
#   Created on 2010-23 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

clear

# Non lancement du script en mode live, car cela ne sert à rien d'installer ce logiciel
if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    echo "Mode live"

    exit 1

fi

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0

nom_distribution="Emmabuntus Debian Edition 5"

base_distribution="bookworm"

dir_install=/opt/ctparental
fichier_init_config=${dir_install}/.init_config_ctparental_tmp.txt
fichier_init_config_final=${dir_install}/.init_config_ctparental.txt
dir_user=/root
file_lightdm=/etc/lightdm/lightdm.conf
file_lightdm_tmp=~/lightdm.conf.tmp
user_group="adm,lp,dialout,fax,cdrom,floppy,tape,audio,dip,video,plugdev,users,netdev,bluetooth,scanner,vboxsf"
user_shell="/bin/bash"
wait_start_ctparental=5
test_website_porn="youporn.com"

user=""
create_compte_enfant="true"
nom_compte_enfant="child"
create_compte_invite="false"

delai_fenetre=20
delai_fenetre_selection=120
delai_fenetre_progression=1200 ; # temporisation de 20 minutes

user=$1
create_compte_enfant=$2
nom_compte_enfant=$3
mdp_compte_enfant=$4
create_compte_invite=$5

echo "user=${user}"
echo "create_compte_enfant=${create_compte_enfant}"
echo "nom_compte_enfant=${nom_compte_enfant}"
echo "mdp_compte_enfant=${mdp_compte_enfant}"
echo "create_compte_invite=${create_compte_invite}"


msg_repository_for=$(eval_gettext 'Repository for')
msg_repository_update=$(eval_gettext 'Repository Update')
msg_install=$(eval_gettext 'Installation')
msg_wait_config=$(eval_gettext 'Waiting for the configuration')
msg_installed=$(eval_gettext 'is already installed')
msg_installed_internet=$(eval_gettext 'was already installed via internet')

install_title=$(eval_gettext 'Installation in progress')
install_text=$(eval_gettext 'Initialization ...')
install_cancel=$(eval_gettext 'Installation canceled.')
install_fail=$(eval_gettext 'Installation failed.')
install_ok=$(eval_gettext 'Installation was successful.')

#Désactivation de Gmenu de Cairo-dock pendant l'installation de logiciel pour éviter les fenêtres de notifications
/usr/bin/cairo_dock_gmenu_off.sh


# Définition des dépôts à ajouter
(

echo "# $msg_repository_update" | sudo tee -a $fichier_init_config
echo "# $msg_repository_update"

# Permet de vérifier la présence des paquets de langue
if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then

    echo "Internet is a live"

    # Suppression de la surveillance de la mise à jour des paquets
    pkill pk-update-icon

    echo "# $msg_repository_update"
    sudo apt-get -qq update
fi

echo "100"
) |
zenity --progress --pulsate \
  --title="$install_title" \
  --text="$install_text" \
  --width=500 \
  --percentage=0 --auto-close --no-cancel \
  --timeout=$delai_fenetre_progression
    if [ $? = "1" ]
    then
      sudo dpkg --configure -a

      #Activation de Gmenu de Cairo-dock
      /usr/bin/cairo_dock_gmenu_on.sh

      zenity --error --timeout=$delai_fenetre \
        --text="$install_cancel"
    else
      sudo dpkg --configure -a

      #Activation de Gmenu de Cairo-dock
      /usr/bin/cairo_dock_gmenu_on.sh

    fi

# Test verrouillage des bases de données apt/dpkg
test_lock=$(sudo lsof /var/lib/apt/lists/lock)
echo "test_lock=${test_lock}"

if [[ $(echo ${test_lock} | grep "lock") ]] ; then

    echo "apt/dpkg database locked"

    exit 2

fi
# Installation des logiciels
(

nom_logiciel_affichage=CTparental
nom_paquet=ctparental*

if [[ $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -P "\tinstall") ]]
then
    echo "# $nom_logiciel_affichage $msg_installed" | sudo tee -a $fichier_init_config
    echo "# $nom_logiciel_affichage $msg_installed"

else
    echo "# $msg_install $nom_logiciel_affichage" | sudo tee -a $fichier_init_config
    echo "# $msg_install $nom_logiciel_affichage"

    # Installation des comptes avant les softs car cela permet une protection plus rapide
    if [[ ! $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -P "\tinstall") ]] ; then

        # Création du compte enfant avec un mot de passe
        if [[ ${create_compte_enfant} == "true" ]] ; then

            if [[ ! -z ${nom_compte_enfant} ]] ; then

                nb_nom_compte_enfant=$(echo -n ${nom_compte_enfant} | wc -w)
                nb_mdp_compte_enfant=$(echo -n ${mdp_compte_enfant} | wc -w)
                tab_mdp_compte_enfant=(${mdp_compte_enfant})
                i=0

                for nom_compte in ${nom_compte_enfant}
                do
                    nom_login="${nom_compte^}"

                    if [[ (${mdp_compte_enfant} != "") && (${nb_nom_compte_enfant} == ${nb_mdp_compte_enfant}) ]] ; then
                        mdp_compte=(${tab_mdp_compte_enfant[${i}]})
                        sudo useradd -p $(perl -e 'print crypt($ARGV[0], "password")' ${mdp_compte}) -G "${user_group}" -s "${user_shell}" -c "${nom_login}" -m ${nom_compte}
                    else
                        sudo useradd -p "" -G "${user_group}" -s "${user_shell}" -c "${nom_login}" -m ${nom_compte}
                    fi

                    # Protection du compte en interdisant la lecture par les autres comptes et groupes
                    sudo chmod -R og-rwx /home/${nom_compte}

                    i=$(($i+1))

                done
            fi
        fi

        # Création du compte invité sans un mot de passe
        if [[ ${create_compte_invite} == "true" ]] ; then

            if [[ $LANG == fr* ]] ; then
                nom_compte="invite"
                nom_login="Invité"
            else
                nom_compte="guest"
                nom_login="Guest"
            fi

            sudo useradd -p "" -G "${user_group}" -s "${user_shell}" -c "${nom_login}" -m ${nom_compte}

            # Protection du compte en interdisant la lecture par les autres comptes et groupes
            sudo chmod -R og-rwx /home/${nom_compte}

            # Mise en place du script pour effacer la session du compte invité lors du lancement
            if test -f ${file_lightdm} ; then

                sed s/.*session-setup-script=[^$]*/session-setup-script=\\/usr\\/bin\\/emmabuntus_reset_session.sh/ ${file_lightdm} > ${file_lightdm_tmp}

                sudo cp ${file_lightdm_tmp} ${file_lightdm}
                rm ${file_lightdm_tmp}
                sudo chmod a+r ${file_lightdm}

            fi

        fi

        # Protection du compte parent en interdisant la lecture par les autres comptes et groupes
        sudo chmod -R og-rwx /home/${user}

        # Désactivation de l'autologin
        if test -f ${file_lightdm} ; then

            sed s/^autologin-user=[^$]*/#autologin-user=/ ${file_lightdm} > ${file_lightdm_tmp}

            sudo cp ${file_lightdm_tmp} ${file_lightdm}
            rm ${file_lightdm_tmp}
            sudo chmod a+r ${file_lightdm}

         fi

    fi

    # Installation des softs
    sudo gdebi -n -q ${dir_install}/dnscrypt-proxy*.deb
    xterm -T "Install CTparental" -e "sudo gdebi -n -q ${dir_install}/ctparental*.deb"

    # Test si l'installation à réussi
    if [[ $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -P "\tinstall") ]] ; then

        echo "# $nom_logiciel_affichage $install_ok" | sudo tee -a $fichier_init_config
        echo "# $nom_logiciel_affichage $install_ok"

        # Permutation des lanceurs entre intall et config
        file="/usr/share/applications/ctparental_install.desktop"
        if test -f ${file} ; then
            sudo mv ${file} ${file}.bak
        fi

        file="/home/${user}/.local/share/applications/ctparental_config.desktop"
        if ! test -f ${file} ; then
            cp /usr/share/applications/ctparental_config.desktop.bak ${file}
            chown ${user}:${user} ${file}
         fi

    else

        echo "# $nom_logiciel_affichage $install_fail" | sudo tee -a $fichier_init_config
        echo "# $nom_logiciel_affichage $install_fail"

        exit 3

    fi

fi

echo "# Attente démarrage $nom_logiciel_affichage"
sleep ${wait_start_ctparental}
#Activation de Gmenu de Cairo-dock
/usr/bin/cairo_dock_gmenu_on.sh

echo "100"

) |
zenity --progress --pulsate \
  --title="$install_title" \
  --text="$install_text" \
  --width=500 \
  --percentage=0 --auto-close --no-cancel \
  --timeout=$delai_fenetre_progression

    if [ $? = "1" ]
    then
      sudo dpkg --configure -a

      #Activation de Gmenu de Cairo-dock
      /usr/bin/cairo_dock_gmenu_on.sh

      zenity --error --timeout=$delai_fenetre \
        --text="$install_cancel"
    else
      sudo dpkg --configure -a
      #Activation de Gmenu de Cairo-dock
      /usr/bin/cairo_dock_gmenu_on.sh

    fi

# Test installation correcte
liste_compte=$(ls /home)
erreur_protection=1

if [[ $(sudo CTparental --help 2> /dev/null | grep "CTparental") ]] ; then
    erreur_protection=0
else
    echo "Protection error install CTparental !!"
    erreur_protection=1
fi

for compte in ${liste_compte}
do

    if [[ ${compte} != ${user} ]] ; then

        test_protection=$(sudo su ${compte} -c "dig ${test_website_porn}")

        if [[ $(echo ${test_protection} | grep "This query has been locally blocked") ]] ; then
            echo "Protection on account (dig) : ${compte}"
        else
            echo "Protection error on account (dig) : ${compte} !!"
            erreur_protection=$(($erreur_protection+1))
        fi

        test_protection=$(sudo su ${compte} -c "ping -w 1 ${test_website_porn}")

        if [[ $(echo ${test_protection} | grep received) ]] ; then
            echo "Protection error on account (ping) : ${compte} !!"
            erreur_protection=$(($erreur_protection+1))
        else
            echo "Protection on account (ping) : ${compte}"
        fi

    fi

done

echo "erreur_protection=${erreur_protection}"

if [ ${erreur_protection} -eq 0 ] ; then

      # Si le fichier utilisateur existe cela veut dire que l'utilisateur ne veut plus qu'on lui pose cette question
      # donc on le désactive pour les futurs utilisateurs
      if [[ ( -f $fichier_init_config ) ]]
      then
        sudo cp -f $fichier_init_config $fichier_init_config_final
      fi

    echo "${install_ok}"

    exit 0

else

    echo "${install_fail}"

    exit 4

fi



