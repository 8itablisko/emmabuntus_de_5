#
# Cette distribution a été conçue pour faciliter le reconditionnement
# des ordinateurs donnés aux associations humanitaires, en particulier
# les communautés Emmaüs et favoriser la découverte de GNU/Linux par
# les débutants, mais aussi prolonger la durée de vie du matériel pour
# limiter le gaspillage entraîné par la surconsommation de matières
# premières.
#
# Cette version a été validée sur une Debian 12 XFCE et LXQt.
#
# Site principal : https://emmabuntus.org/
#
# Copyright (C) 2010-23 Collectif Emmabuntüs (contact@emmabuntus.org).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---------------------------------------------------------------------------------------------------------------------------------
ChangeLog.txt
---------------------------------------------------------------------------------------------------------------------------------

Modifications de la version RC 1 par rapport à la Alpha 1 du 05/05/2023 :
- Basée sur la Debian 12 RC 2
- Ajout langues supplémentaires : ticket 21
- Ajout de Jami à partir du dépot Debian : ticket 22
- Mise à jour : Firefox 102.10.0esr, Thunderbird 102.10.0, Ventoy 1.0.91, Pulseaudio Equalizer Ladspa 2022.07


Modifications de la version Alpha 1 par rapport à la Alpha 0 du 06/04/2023 :
- Basée sur la Debian 12 RC 1
- Ajout des utilitaires Deb-get et Deborah : ticket 14
- Ajout du contrôle parental CTparental : ticket 16
- Correction création cache apt
- Utilisation de gnome-packagekit à partir du dépôt
- Amélioration GRUB en 1024x768 et message sur l'installation de Calamares
- Filezilla en version 64 bits uniquement présent sur Debian 12
- Suppression de Childsplay, Omnitux-ligth, Wammu, System-config-samba à cause de Python 2 obsolète : ticket 15
- Suprression de Samba, bind9
- Désactivation HTTPS en mode normal dans Firefox
- Désactivation de umask par défaut non sécurisé (fichiers lisibles par tout le monde)
- Mise à jour : Firefox 102.9.0esr, Thunderbird 102.9.0, Ventoy 1.0.90, Warpinator 1.4.5, Les cahiers du débutant Debian Bullseye 11.4, TurboPrint 2.54-1, Boot-Repair 4ppa200


Modifications de la version Alpha 0 par rapport à la EmmaDE4 1.01 du 17/07/2022 :
- Basée sur la Debian 12 version en développement
- Ajout config apt-cacher-ng
- Ajout du boot en UEFI 64 bits avec une ISO 32 bits et vice-versa
- Ajout installation du boot en UEFI 64 bits avec une ISO 32 bits et vice-versa
- Ajout du lancement MenTest86+ en mode UEFI
- Correction activation non lancement sélection clavier dans SysLinux et GRUB
- Correction crash Zenity lors de l'installation des nouveaux paquets de langues
- Correction script de changement de langues à cause de Calamares
- Mise à jour des cahiers du débutant Debian 11.3
- Mise à jour du diaporama de Calamares par ajout d'une diapo sur la campagne de réemploi
- Mise à jour d'une nouvelle version de DWService agent et mise en place d'un paquet
- Mise à jour gSpeech 0.11.0
- Suppression MultiSystem devenu obsolète : ticket 5
- Suppression des extensions Disconnect, HTTPS Everywhere et utilisation des fonctions de Firefox : ticket 6
- Suppression MintLocale devenu non compatible avec Debian 12
- Suppression de Hexchat et d'Empathy : ticket 9
- Suppression de Soundconverter et de Winff : ticket 10
- Suppression d'Imagination et de Hugin : ticket 11
- Suppression de Stellarium pour gain de place sur l'ISO : ticket 11
- Suppression de ndiswrapper et de ndisgtk obsolètes
- Suppression de Dominos qui ne fonctionne plus
- Utilisation de la fonte Liberation Sans par défaut dans LXQt : ticket 6
- Utilisation de gnome-packagekit version Bullseye (TODO)
- Activation de boot-repair et os-uninstaller uniquement en live
- Mise à jour : Firefox ESR 91.11.0, Thunderbird 91.10.0, Ventoy 1.0.78, Warpinator 1.2.9, Radiotray-NG 0.2.8, TurboPrint 2.53-1, boot-repair 4ppa200, Veracrypt 1.25.9, MintStick 1.4.9, Cahiers du débutant Debian 11.3

