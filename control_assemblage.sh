#! /bin/bash

file=${1}

if [[ ${file} = "" ]] ; then

    # Si pas de fichier sélection du plus récent
    file=$(ls *.log | tail -1)

fi

echo "Script de contrôle de l'assemblage du fichier de log : ${file}"
echo ""

if [ -f ${file} ] ; then

    cat ${file} | grep "Failed to satisfy all dependencies"
    cat ${file} | grep "Dependency is not satisfiable"
    cat ${file} | grep "E: Package"
    cat ${file} | grep "E: Unable to correct problems, you have held broken packages"
    cat ${file} | grep "This package is uninstallable"
    cat ${file} | grep "dependency problems - leaving unconfigured"
    cat ${file} | grep "is not installed"
    cat ${file} | grep "error, file not found"
    cat ${file} | grep "failed "
    cat ${file} | grep "error "
    cat ${file} | grep "E: Unable to locate package"
    cat ${file} | grep "Removing" | grep -Fv -e "dctrl-tools" -e "firmware-netronome" -e "memtest86+" -e "isolinux" -e "syslinux-common " -e "loadlin" -e "shim-signed" -e "grub-efi-amd64-bin" -e "grub-efi-ia32-bin" -e "isolinux" -e "xorriso" -e "zsync" -e "squashfs-tools" -e "grub-efi-amd64-signed" -e "shim-helpers-amd64-signed" -e "grub-efi-ia32-signed" -e "shim-helpers-i386-signed" -e "shim-unsigned" -e "libisoburn1"

else

    echo "Le fichier n'existe pas"

fi

echo ""
echo "Fin du contrôle de l'assemblage"
